export function query(selector, scope) {
	if(scope === null) throw 'query scope cannot be null';
	return (scope || document).querySelector(selector);
}

export function listen(target, type, callback, capture) {
	target.addEventListener(type, callback, !!capture);
}


// Delegate event is a global function for calling all type listeners in one single place
// it's more efficient, instead to add event listener for every element by itself
export function delegateEvent(target, selector, type, handler, capture) {
	const dispatchEvent = event => {
		const targetElement = event.target;
		const potentialElements = target.querySelectorAll(selector);
		let i = potentialElements.length;

		while (i--) {
			if (potentialElements[i] === targetElement) {
				handler.call(targetElement, event);
				break;
			}
		}
	};

	listen(target, type, dispatchEvent, !!capture);
}

export const escapeForHTML = s => {
	const chars = {'<': '','>': '', '&': '&amp;'};
	return s.replace(/[&<>]/g, m => chars[m]);
}

export function makeRequest(url = '', method = 'GET', data = {}, headers = {}, callback) {
	fetch(url, {method, headers})
	.then(response => {
		return response.json()
	})
	.then(response => {
		callback && typeof callback === 'function' && callback({status: true, data: response})
	})
	.catch(err => {
		throw `Err: ${err}`
	})
}

