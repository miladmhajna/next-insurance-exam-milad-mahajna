# Your broken Tasks Widget
Complete as many tasks as can before deadline.
The order is up to you :)
Feel free to remove code and implement your own version, or change it anyway you like.
You can use any resource you like, and ask us questions during the session. 

## Tasks:
1.  Tasks should persist after closing the browser. ( 5pt )
    ## Store save to localStorage

2.  Prevent duplication in tasks. ( 10pt ) 
    ## add validation before addItem

3.  Fix the `clear completed` action in the footer ( 15pt ) 
    ## change removeCompletedItems function when value is false, Filter and keep uncompleted.

4.  After doubleClick on an item, clicking enter should exit "edit" (10pt)

    ## Hanlde event delegate for (li .edit), and when key enter pressed, the blur event

5.  Fix Style issue when inserts a lot of items - add short description for how you approach it (5pt)

    ## .todo-list already have max-height, so just need to adjust overflow to auto.

6.  When launching the app for the first time, the `addItem` input should be in focus (5pt)

    ## add focus for the input in View constructor.

7.  Fix the counter on the footer (5pt)
    ## fix the passed value in setItemsLeft function from static 0 to variable active

8.  Add a code comment explaining what `delegateEvent` function does (10pt)

    ## // Delegate event is a global function for calling all type listeners in one single place
    ## // it's more efficient, instead to add event listener for every element by itself

9.  Load and display tasks from https://jsonplaceholder.typicode.com/todos when application loads (20pt)

    ## fetch request created in store
    ## need async, await,

10. Prevent adding task that contain `< >` in the title. (replace with blank), For example: 'Learn < JS >' should become 'Learn JS' (10pt) 

  ## change escapeForHTML function logic
  
11. Handle the error in the console (15pt)

    ## remove null in function updateFilterButtons


Good luck! 
